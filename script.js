let container = document.getElementsByClassName('container')[0]
let criarTorre = document.createElement('div')
criarTorre.id = ('torre1')
criarTorre.className = ('blocos')
container.appendChild(criarTorre)
let criarDiscos = document.createElement('div')
criarDiscos.id = ('disco1')
criarDiscos.className = ('disco')
criarTorre.appendChild(criarDiscos)
let criarDiscos2 = document.createElement('div')
criarDiscos2.id = ('disco2')
criarDiscos2.className = ('disco')
criarTorre.appendChild(criarDiscos2)
let criarDiscos3 = document.createElement('div')
criarDiscos3.id = ('disco3')
criarDiscos3.className = ('disco')
criarTorre.appendChild(criarDiscos3)
let criarDiscos4 = document.createElement('div')
criarDiscos4.id = ('disco4')
criarDiscos4.className = ('disco')
criarTorre.appendChild(criarDiscos4)
let criarTorre2 = document.createElement('div')
criarTorre2.className = ('torre')
container.appendChild(criarTorre2)
let criarTorre3 = document.createElement('div')
criarTorre3.className = ('torre')
container.appendChild(criarTorre3)
let criarBlocoTorre2 = document.createElement('div')
criarBlocoTorre2.id = ('torre2')
criarBlocoTorre2.className = ('blocos')
criarTorre2.appendChild(criarBlocoTorre2)
let criarBlocoTorre3 = document.createElement('div')
criarBlocoTorre3.id = ('torre3')
criarBlocoTorre3.className = ('blocos')
criarTorre3.appendChild(criarBlocoTorre3)

const blocos = document.getElementsByClassName("blocos");
const tower1 = document.getElementById("torre1");
tower1.addEventListener("click", (x) => jogo(x));
const tower2 = document.getElementById("torre2");
tower2.addEventListener("click", (x) => jogo(x));
const tower3 = document.getElementById("torre3");
tower3.addEventListener("click", (x) => jogo(x));

const modal = document.getElementById("modal");

let primeiroDisco
let tamanhoDisco1
let tamanhoDisco2
let idTorre
let torreAnterior 
let jogada = ""

function jogo(x) {
    if(jogada === "") {
        jogada = "disco"
    } else {
        jogada = "torre"
    }

    console.log(jogada)

    let torreBloco = x.currentTarget
    let numeroDiscos = torreBloco.childElementCount
    let torreAtual = torreBloco.id

    if(jogada === "disco"){
        primeiroDisco = torreBloco.lastElementChild
        if (primeiroDisco === null) {
            alert("Escolha uma torre com discos.")
            jogada = ""
        }else {
            primeiroDisco.style.border = "2px solid whitesmoke"
            tamanhoDisco1 = primeiroDisco.clientWidth
            torreAnterior = torreAtual
            desabilitar(torreAtual)
        }
    }


    if(jogada === "torre") {
        if(numeroDiscos !== 0) {
            let discoEmCima = torreBloco.lastElementChild
            tamanhoDisco2 = discoEmCima.clientWidth
        }

        if(numeroDiscos === 0 || tamanhoDisco1 < tamanhoDisco2) {
            torreBloco.appendChild(primeiroDisco)
            primeiroDisco.style.border = "none"
            habilitar(torreAnterior)
            jogada = ""
        }
        else if(numeroDiscos === 0 || tamanhoDisco1 > tamanhoDisco2) {
            alert("Não coloque um bloco maior em cima de um menor.")
            primeiroDisco.style.border = "none"
            habilitar(torreAnterior)
            jogada = ""
        }

    }

    vitoria()

}
function desabilitar(torre) {
    let torreAtual = document.getElementById(torre)
    torreAtual.style.pointerEvents = "none"
}
function habilitar(torre) {
    let torreAnterior = document.getElementById(torre)
    torreAnterior.style.pointerEvents = "auto"
}

function vitoria() {
    let ultimaTorre = document.getElementById("torre3")

    let blocos = ultimaTorre.childElementCount
    if(blocos === 4){
        modal.style.display = "flex"
    }
}

const egg = document.getElementById("egg")
const hudson = document.getElementById("hudson")
const audio = document.querySelector("audio")

hudson.addEventListener("click", function() {
    egg.style.display = "flex";
    audio.play();
})